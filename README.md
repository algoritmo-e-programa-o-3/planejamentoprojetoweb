# PlanejamentoProjetoWeb

## Análise de Requisitos e Especificação
### 1.1. Requisitos Funcionais 

* [RF 001] Login: O sistema deverá exigir login (com usuários e senhas individuais) para que se tenha um controle de acessos e permissões;
* [RF 002] Dasboard: Área restrita para o cliente admin do sistema.
* [RF 003] Cadastro de Viagens: O sistema vai ter uma opção para que sejam cadastrados as excursões que a empresa disponibilizara para os clientes.
* [RF 003.1] O sistema vai permitir que o empresário cadastre fotos de excursões futuras e excursões realizadas, bem como data de partida e chegada da viagem e também o local de saída da excursão(cidade) e o destino e os detalhes do passeio.
* [RF 003.2] O sistema vai permitir que o empresário cadastre valores para as viagens, e a forma de pagamento.
* [RF 004] Atualização de Viagens: O sistema vai ter uma opção para  atualizar os dados (usados no cadastramento das viagens); 
* [RF 0004.1] O sistema vai ter uma opção para o empresário alterar as fotos das excursões, atualizar as datas das viagens, o local de saída e o destino e também o detalhes do passeio.
* [RF 0004.2] O sistema vai ter uma opção para o empresário alterar valores e a forma de pagamento
* [RF 005] Deletar Viagens: O sistema deverá ter uma opção para o deletar os dados (usados no cadastramento das viagens); 
* [RF 0005.1] O sistema vai ter uma opção para o empresário deletar as fotos das excursões e detalhes da viagem .
* [RF 0005.2] O sistema vai ter uma opção para o empresário deletar valores e a forma de pagamento da viagem.
* [RF 006] Cadastro de Rotas diárias das viagens dos estudantes: O sistema vai ter uma opção para que sejam cadastrados a rotina das viagens diárias tanto no turno da manha como a noite.
* [RF 006.1] O sistema vai permitir que o empresário cadastre à hora da partida(Piratini) e de chegada(Pelotas), bem como os pontos de parada e horário  para pegar os alunos. 
* [RF 006.2] O sistema vai permitir que o empresário cadastre valores para as viagens diárias, semanais e mensais.
* [RF 007] Atualização de Rotas diárias das viagens dos estudantes: O sistema vai ter uma opção para que sejam atualizadas a rotina das viagens diárias tanto no turno da manha como a noite.
* [RF 007.1] O sistema vai permitir que o empresário atualize à hora da partida(Piratini) e de chegada(Pelotas), bem como os pontos de parada e horário  para pegar os alunos. 
* [RF 007.2] O sistema vai permitir que o empresário atualize valores para as viagens diárias, semanais e mensais.
* [RF 008] Deletar Rotas diárias das viagens dos alunos: O sistema deverá ter uma opção para o deletar os dados (usados no cadastramento das viagens);
* [RF 0008.1] O sistema vai ter uma opção para o empresário deletar valores diários, semanais e mensais.
* [RF 009] Inserir comentários sobre as viagens: O sistema vai ter uma opção para que o usuário possa cadastrar comentários sobre as viagens.
* [RF 009.1] O sistema vai permitir que o usuário avalie a viagem na qual foi com a empresa (avaliação vai ser de 0-5, sendo 0 a pior avaliação e 5 a melhor) e também vai ter um campo de texto para o usuário descrever sua experiência na viagem.

### 1.2. Requisitos Não Funcionais 

* [RNF 001] Cadastro Empresário: O sistema vai dispor de uma opção de cadastro do empresário afim de definir quem terá acesso ao sistema;
* [RNF 003] Receber o comentário : O sistema vai dispor de uma opção para o empresário receber o comentário(depoimento) do usuário e assim avaliara se esse comentário será apresentado na tela de front para não afetar a imagem da empresa.

### 2.1. Telas Adobexd

* https://xd.adobe.com/spec/24ecf10c-c815-490b-4dad-b27d1e0c01a1-52dd/
* https://xd.adobe.com/view/0983a634-7e40-4aad-7cda-c99e2c757078-7fb2/

### 3.1. Diagrama ER

